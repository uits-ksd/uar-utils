import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


def getenv(key, default):
    value = os.getenv(key)
    if value is None or len(value) == 0:
        return default
    return value


# Input entries here along with default values. If the env is not set
# then default is used.  If default is not set, then error will occur.
# This most likely what is wanted to indicate that env should be set to
# hide sensitive info.
KRAOWNER_USERNAME = getenv("KRAOWNER_USERNAME", default="")
KRAOWNER_PASSWORD = getenv("KRAOWNER_PASSWORD", default="")

DATAPUMP_USERNAME = getenv("DATAPUMP_USERNAME", default="")
DATAPUMP_PASSWORD = getenv("DATAPUMP_PASSWORD", default="")

DATABASE_PORT = getenv("DATABASE_PORT", default=1512)
DATABASE_NAME = getenv("DATABASE_NAME",
                       default="UARPRDEX")

DATABASE_SERVICENAME = getenv("DATABASE_SERVICENAME",
                              default="UARPRDEX")

DATABASE_URI = getenv("DATABASE_URI",
                      default="c6x31oanmaim.us-west-2.rds.amazonaws.com")

DATABASE_ENCRYPTKEY = getenv("DATABASE_ENCRYPTKEY", default="")

ENCODING = getenv("ENCODING", default="UTF-8")
DSN = DATABASE_NAME+"."+DATABASE_URI+"/"+DATABASE_SERVICENAME

S3_BUCKET = getenv("S3_BUCKET", default="test-bucket")
S3_REGION = getenv("S3_REGION", default="us-east-1")

LOGGING_OUTPUTDIR = getenv("LOGGING_OUTPUTDIR", default="logs")
LOGGING_ERRORFILENAME = getenv("LOGGING_ERRORFILENAME",
                               default="uasaas_error.log")

LOGGING_LOGFILENAME = getenv("LOGGING_LOGFILENAME",
                             default="uasaas.log")