# AU Utilities codes #


### Code layout ###
These utilities contains sub-folders under uautils folder.  The uautils module currently consists of a logger, s3util, settings, and test sub-modules as show in the image below.   Additionally, md_images folder contains images for this Readme.md file.

>>>> ![Code layout](./md_images/code-layout.png)

Along the previous mentioned folders, uautils contains **\__init__.py**, **.env**, **.gitignore**, **README.md**, and **requirements.txt**.  
- **\__init__.py** - The dunder init is use to indicate that the folder is a module.
- **.env** - This file contains reference to environment settings or default settings values.
- **.gitignore** - This is used to exclude file and folder from repository.
- **README.md** - Contains documentation for this code repository.
- **requirements.txt** - Generate from Pipfile of pipenv virtual environment.

#### settings ####
The **settings** module contains **\__init__.py** and **settings.py** as shown below.

>>>>![settings-files](./md_images/settings-files.png)

- **\__init__.py** - As previously mentioned, this file is to indicate settings is a modules.
- **settings.py** - Implemented to returns the settings within the environment or within the settings file.  **settings.py** make use of ***dotenv*** library.  As shown in the code below, the default is used for setting information that are not critical or have security concerns, whereas, for settings that should be hidden are set in the environment.

>>>>![settings](md_images/settings.png)


#### Logger ####
The logger sub-module contains **\__init__.py** and **loggerinitialize.py** as shown in the image below.

>>>> ![logger-files](./md_images/logger-files.png)

- **\__init__.py** - Like settings, this file indicates that the logger folder is a sub-module.  
- **loggerinitializer.py** - is a utility that handles all logging as shown in the code below.
The function ***initialize\_logger*** sets up the logging format. The format starts with asciitime, logging level, the name of the current handler, and the logging message.
Along with the format, the ***initialize\_logger*** have three handlers for logging.  There are stream, error file, and conventional file handler.  The output directory where
the logs are written to are set in the module settings which is discussed later in this document. The settings consists of the output directory, name of the error log file,
and the conventional log file name. These are shown in the code at line **20** and **26** below.

>>>> ![loggerinitializer](./md_images/logger-initializer.png)

#### s3util ####
Like previous modules, s3util also contains **\__init__.py** along with **s3util.py** as shown in the image below.

>>>>![s3util-files](./md_images/s3util-files.png)

- **\__init__.py** - As with logger and other pervious mentioned modules, this file is used to indicate that s3util is a module.
- **s3util.py** - contains a utility class ***S3Util*** with an ***upload()*** function.  The constructor of ***S3Util*** expects parameters ***content*** and ***filename***.  The 
***content*** parameter is the body of the object in S3 and the ***filename*** will be used as the key as shown in the code below. The s3util uses ***boto3*** library to connect and upload to S3.  The code is self explanatory. Any error occur, the code will log the error, otherwise it will return the response received from S3.

>>>>![s3util](./md_images/s3util.png)

#### logs ####
This folder contains logs.  As discussed in the logger section previously, the output from the logger module is written to this folder.  Although the folder is called logs, this can be changed in the settings module.  Logger will get the name of the folder from settings. Along with the name of the log's folder, the error log file name and the general log file name also come from settings as shown in the image below.  Notice the **.log** are post-pended with a number.  This is part of the logger setting.  

>>>>[logs](md_images/logs.png)

- **.keep** - This file is used to force git to allow the folder to be checked in since git does allows empty folder.


#### test ####
Test folder contains logs folder, exportenv.sh bash script, s3test.py and settingstest.py as shown below.

>>>>![test-files](md_images/test-files.png)

- **logs** - This folder under test is similar to the previous mentioned log folder.
- **exportenv.sh** - The bash script is used to set username and password in the running environment.  This is for testing purposes only.
- **s3test.py** - This test make use of the **moto** library. **moto** is use to mock AWS S3 bucket.  The code connect to the **moto** virtual mock S3 bucket and sends a test object to the bucket and read the object back out of the bucket.
- **settingstest.py** - This test is use to test the environment settings.
