import unittest 
import boto3
from moto import mock_s3

from settings import settings
from s3util import S3Util

import sys
import os
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))


@mock_s3
class TestS3(unittest.TestCase):

    def test_upload(self):
        bucket = settings.S3_BUCKET
        s3_client = boto3.client('s3')
        s3_client.create_bucket(Bucket=bucket)

        # Mock an s3 bucket since Moto is a virtual AWS account
        s3util = S3Util('This is a test', 'mytest.txt')
        s3util.upload()

        # Reread the object from the bucket and assert the content
        body = s3_client.get_object(Bucket=bucket, Key='mytest.txt')['Body'].read()
        assert 'This is a test' == body.decode("utf-8")


if __name__ == '__main__':
    unittest.main()
