import logging
import unittest

import sys
import os
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from settings import settings
logger = logging.getLogger(__name__)

class SettingsTest(unittest.TestCase):
    def test_kraowner_username(self):
        self.assertEqual("U53rn@me", settings.KRAOWNER_USERNAME)
    
    def test_kraowner_password(self):
        self.assertEqual("K4a0wneR", settings.KRAOWNER_PASSWORD)

    def test_datapump_username(self):
        self.assertEqual("ABC@123", settings.DATAPUMP_USERNAME)

    def test_datapump_password(self):
        self.assertEqual("X1z@ABC", settings.DATAPUMP_PASSWORD)

    def test_database_port(self):
        self.assertEqual(1512, settings.DATABASE_PORT)

    def test_database_name(self):
        self.assertEqual("UARPRDEX", settings.DATABASE_NAME)

    def test_database_servicename(self):
        self.assertEqual("UARPRDEX", settings.DATABASE_SERVICENAME)

    def test_database_uri(self):
        self.assertEqual("c6x31oanmaim.us-west-2.rds.amazonaws.com", settings.DATABASE_URI)

    def test_database_encryptkey(self):
        self.assertEqual("X0Hnca", settings.DATABASE_ENCRYPTKEY)

    def test_encoding(self):
        self.assertEqual("UTF-8", settings.ENCODING)

    def test_dsn(self):
        self.assertEqual("UARPRDEX.c6x31oanmaim.us-west-2.rds.amazonaws.com/UARPRDEX", settings.DSN)

    def test_s3_bucket(self):
        self.assertEqual("test-bucket", settings.S3_BUCKET)

    def test_s3_region(self):
        self.assertEqual("us-east-1", settings.S3_REGION)

if __name__=='__main__':
    unittest.main()