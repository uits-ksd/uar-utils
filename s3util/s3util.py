import logging
import boto3
from botocore.exceptions import ClientError


from settings import settings
from logger.loggerinitializer import initialize_logger

initialize_logger()
logger = logging.getLogger(__name__)


class S3Util(object):
    ''' S3 utility for uploading data to S3 bucket '''
    def __init__(self, content, filename):
        self.content = content
        self.filename = filename

    def upload(self):
        response = None

        try:
            s3 = boto3.client('s3', region_name=settings.S3_REGION)
            response = s3.put_object(Bucket=settings.S3_BUCKET, 
                                     Key=self.filename, Body=self.content)
            logger.info("The put object response: ", response)
        except ClientError as e:
            logger.error(e)
            return e

        return response
