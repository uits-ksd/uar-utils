import logging
from logging.handlers import RotatingFileHandler
from settings import settings
import os


def initialize_logger():
    # Initialize logger for general usage
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s : %(levelname)s : %(name)s : %(message)s')

    # Setting up stream handler for logger
    streamhandler = logging.StreamHandler()
    streamhandler.setLevel(logging.INFO)
    streamhandler.setFormatter(formatter)
    logger.addHandler(streamhandler)

    # Setting up file handler for logger - this is mainly for error logging
    filehandler = logging.FileHandler(os.path.join(settings.LOGGING_OUTPUTDIR,
                                      settings.LOGGING_ERRORFILENAME), "w",
                                      encoding=None, delay="true")
    
    filehandler.setLevel(logging.ERROR)
    filehandler.setFormatter(formatter)
    logger.addHandler(filehandler)

    # Setting up rotating file handler for logger for debugging
    rotatinghandler = RotatingFileHandler(os.path.join(
        settings.LOGGING_OUTPUTDIR, settings.LOGGING_LOGFILENAME),
        maxBytes=2000, backupCount=10)
    
    rotatinghandler.setLevel(logging.DEBUG)
    rotatinghandler.setFormatter(formatter)
    logger.addHandler(rotatinghandler)